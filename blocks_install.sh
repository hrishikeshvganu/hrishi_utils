sudo apt-get install python-numpy python-scipy python-dev python-pip python-nose g++ libopenblas-dev git gfortran libhdf5-dev graphviz
sudo pip install Theano
sudo pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git
sudo pip install git+git://github.com/mila-udem/fuel.git
sudo pip install git+git://github.com/mila-udem/blocks.git
sudo chown -R  ubuntu  /home/ubuntu/.theano/
