#Show tracking information
git remote show origin
#set local branch to track remote branch for pulls and pushes. "origin" denotes remote
git branch --set-upstream-to=origin/post_integ_attn_dev post_integ_attn_dev
